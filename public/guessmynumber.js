var randomNumber = 0;
var guessesSpanLog = " ";
var numberOfguessesSpan = 0;

randomNumber=Math.floor(Math.random()*101);

document.getElementById('guessForm').addEventListener('submit', function(e){
	e.preventDefault();

})

document.getElementById('guessbox').addEventListener('keypress', function(e){
	var key = e.which || e.keyCode;
	if (key == 13) {
		processGuess(this.form);
	}
})


function processGuess (form) {

	var guess = form.inputbox.value;
	guessesSpanLog = guessesSpanLog + guess + " ";
	document.getElementById("guessesSpan").textContent=guessesSpanLog;

	if (guess == randomNumber)
	{
		document.getElementById("feedbackSpan").textContent="Correct!";
		document.getElementById("guessForm").style.display="none";
		document.getElementById("guessesSpan").style.display="none";
		document.getElementById("reloadButtonForm").style.display="block";
		document.getElementById("dog").style.display="block";
		
	}

	if (guess > randomNumber)
	{
		document.getElementById("feedbackSpan").textContent="Lower!";
	}

	if (guess < randomNumber)
	{
		document.getElementById("feedbackSpan").textContent="Higher!";
	}
	document.getElementById("attemptsSpan").textContent= "Attempts: " + ++numberOfguessesSpan;

	document.getElementById('guessbox').value="";


}

function reloadPage () {
	window.location.reload(false);
}



