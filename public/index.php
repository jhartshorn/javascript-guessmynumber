<!DOCTYPE HTML>
<html>
	<head>
		<title>Guess My Number</title>

		<link href="bower_components/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
	</head>

	<body>

		<div class="container">

			<div class="header clearfix">
				<h3 class="text-muted">James Hartshorn</h3>
			</div>

			<div class="jumbotron">

				<h2>Guess My Number!</h2>

				<span class="lead" id="feedbackSpan">Guess a number from zero to one hundred.</span><br>

				<form class="hideOnGameOver" id="guessForm" onsubmit="processGuess(this.form)">
					<input type="text" id="guessbox" name="inputbox" value="" autocomplete="off"><br><br>
					<input id="guessbutton" class="btn btn-lg btn-success" TYPE="button" NAME="button" Value="Guess" onClick="processGuess(this.form)">
				</form>

				<form style="display: none" id="reloadButtonForm">
					<input class="btn btn-lg btn-success" TYPE="button" NAME="button" Value="Play Again" onClick="reloadPage()">
				</form>

				<span class="lead hideOnGameOver" id="guessesSpan"></span><br>
				<span class="lead" id="attemptsSpan"></span>
				<img src="dog.jpg" style="display: none" id="dog">

			</div>
		</div>

		<script type="text/javascript" src="guessmynumber.js"></script>

	</body>

</html>

